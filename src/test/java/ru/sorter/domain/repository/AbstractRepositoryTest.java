package ru.sorter.domain.repository;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ru.sorter.SpringContext;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.Shelving;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
public abstract class AbstractRepositoryTest extends SpringContext {
    @Autowired
    BookRepository repository;
    @Autowired
    ShopRepository shopRepository;

    EasyRandom randomizer = new EasyRandom();
    Book book;
    Shelving shelving;

    @Test
    void hasRepo() {
        assertNotNull(repository);
        assertNotNull(shopRepository);
    }

    protected Shelving createShelving() {
        var generated = randomizer.nextObject(Shelving.class);

        generated.setBooks(null);
        generated.setId(null);
        return shopRepository.saveAndFlush(generated);
    }

    protected Book createBook(UUID shelvingId) {
        var generated = randomizer.nextObject(Book.class);
        generated.setId(null);

        Stream.of(
                Optional.ofNullable(shelvingId),
                Optional.ofNullable(shelving).map(Shelving::getId),
                Optional.empty())
                .flatMap(Optional::stream)
                .findFirst()
                .map(it -> (UUID) it)
                .ifPresent(generated::setShelvingId);
        return repository.saveAndFlush(generated);
    }

}
