package ru.sorter.domain.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.Shelving;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BookRepositoryTest extends AbstractRepositoryTest {

    @BeforeEach
    public void setup() {
        shelving = createShelving();
        book = createBook(shelving.getId());
    }

    @Test
    void findByIdTest() {
        var result = repository.findById(book.getId());
        assertNotNull(result);
        assertEquals(result.get(), book);
    }

    @Test
    void findAllByLevelTest() {
        var result = repository.findAllByLevel(book.getLevel());
        assertNotNull(result);
        assertEquals(result.size(), 1);
        assertEquals(((List) result).get(0), book);
    }

    @Test
    void findByNameTest() {
        var result = repository.findAllByName(book.getName());
        assertNotNull(result);
        assertEquals(result, List.of(book));
    }

    @AfterEach
    void tearDown() {
        Optional.ofNullable(shelving)
                .map(Shelving::getBooks)
                .stream()
                .flatMap(Collection::stream)
                .map(Book::getId)
                .forEach(repository::deleteById);
        shopRepository.deleteById(shelving.getId());
    }

}

