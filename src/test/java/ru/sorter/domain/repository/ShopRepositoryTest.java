package ru.sorter.domain.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ShopRepositoryTest extends AbstractRepositoryTest {

    private List books;

    @BeforeEach
    public void setup() {
        var shelvingWithBook = createShelving();
        books = new ArrayList();
        books.add(createBook(shelvingWithBook.getId()));
        books.add(createBook(shelvingWithBook.getId()));
        books.add(createBook(shelvingWithBook.getId()));
        books.add(createBook(shelvingWithBook.getId()));
        shelvingWithBook.setBooks(books);

        shelving = shopRepository.saveAndFlush(shelvingWithBook);
    }

    @Test
    void findById() {
        var result = shopRepository.findById(shelving.getId());
        assertNotNull(result);
        assertNotNull(result.get());
        assertEquals(result.get(), shelving);
        assertNotNull(result.get().getBooks());
        assertEquals(result.get().getBooks().size(), books.size());
    }

    @AfterEach
    void tearDown() {
        shelving.getBooks().forEach(it->repository.deleteById(it.getId()));
        shopRepository.deleteById(shelving.getId());
    }

}

