package ru.sorter.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.sorter.SpringContext;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.Shelving;
import ru.sorter.domain.repository.BookRepository;
import ru.sorter.domain.repository.ShopRepository;
import ru.sorter.exception.EntityNotFoundException;
import ru.sorter.mapper.BookDtoMapper;
import ru.sorter.mapper.BookMapper;
import ru.sorter.service.ICommonService;
import ru.sorter.web.dto.BookDto;
import ru.sorter.web.dto.enums.Level;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = BookingController.class, secure = false, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
                BookMapper.class,
                BookDtoMapper.class,
                ICommonService.class
        })
})
public class BookingControllerTest extends SpringContext {

    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private ShopRepository shopRepository;

    private BookMapper bookMapper = Mappers.getMapper(BookMapper.class);

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    private static final String URL = "/booking/api/v1/books";

    private static EasyRandom randomizer = new EasyRandom();
    private BookDto bookDto;
    private Book book;
    private Shelving shelving;

    @BeforeEach
    void setUp() {
        bookDto = randomizer.nextObject(BookDto.class);
        book = bookMapper.apply(bookDto);
        shelving = randomizer.nextObject(Shelving.class);
        book.setShelvingId(shelving.getId());
        shelving.setBooks(List.of(book));
        when(bookRepository.findById(bookDto.getId())).thenReturn(Optional.of(book));
        when(bookRepository.findAllByName(bookDto.getName())).thenReturn(List.of(book));
        when(bookRepository.findAllByLevel(any())).thenReturn(List.of(book));
        when(bookRepository.saveAndFlush(any(Book.class))).then(arg -> arg.getArgument(0));
        when(shopRepository.saveAndFlush(any(Shelving.class))).then(arg -> arg.getArgument(0));
        when(shopRepository.findById(eq(shelving.getId()))).thenReturn(Optional.of(shelving));
    }


    @Test
    void getBookTest() throws Exception {
        mockMvc.perform(get(URL + "/{bookId}", bookDto.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(book.getId().toString())))
                .andExpect(jsonPath("$.name", is(book.getName().toString())))
                .andExpect(jsonPath("$.shelvingId", is(book.getShelvingId().toString())))
                .andExpect(jsonPath("$.level", is(book.getLevel().toString())));
    }

    @Test
    void getBookNotFoundTest() throws Exception {
        mockMvc.perform(get(URL + "/{bookId}", UUID.randomUUID()))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(mvcResult -> Objects.requireNonNull(mvcResult.getResolvedException()).getClass().isAssignableFrom(EntityNotFoundException.class));
    }

    @ParameterizedTest
    @EnumSource(Level.class)
    void getBooksByParamsTest(Level level) throws Exception {
        book.setLevel(ru.sorter.domain.entity.enums.Level.byName(level.name()));
        mockMvc.perform(get(URL)
                .param("shelvingId", shelving.getId().toString())
                .param("level", level.name()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(book.getId().toString())))
                .andExpect(jsonPath("$[0].name", is(book.getName())))
                .andExpect(jsonPath("$[0].shelvingId", is(book.getShelvingId().toString())))
                .andExpect(jsonPath("$[0].level", is(book.getLevel().toString())));
    }

    @Test
    void getBooksByParamsBadRequestTest() throws Exception {
        mockMvc.perform(get(URL)
                .param("shelvingId", bookDto.getShelvingId().toString())
                .param("level", "LEVEL"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void getBookByNameTest() throws Exception {
        mockMvc.perform(get(URL + "/name")
                .param("value", bookDto.getName()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(book.getId().toString())))
                .andExpect(jsonPath("$.name", is(book.getName())))
                .andExpect(jsonPath("$.shelvingId", is(book.getShelvingId().toString())))
                .andExpect(jsonPath("$.level", is(book.getLevel().toString())));
    }

    @Test
    void createBookTest() throws Exception {
        when(shopRepository.saveAndFlush(any())).thenReturn(shelving);
        when(bookRepository.saveAndFlush(any())).thenReturn(book);
        BookDto bookDto = BookDto.builder().name("Азбука").build();
        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(bookDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.name", notNullValue()))
                .andExpect(jsonPath("$.shelvingId", notNullValue()))
                .andExpect(jsonPath("$.level", notNullValue()));
    }

    @Test
    void failCreateBookTest() throws Exception {
        BookDto bookDto = BookDto.builder().name("Азбука").build();
        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(bookDto)))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(mvcResult -> Objects.requireNonNull(mvcResult.getResolvedException()).getClass().isAssignableFrom(EntityNotFoundException.class));
        ;
    }

    @Test
    void deleteBookTest() throws Exception {
        mockMvc.perform(delete(URL + "/{bookId}", bookDto.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isAccepted());
    }

    @Test
    void putBookTest() throws Exception {
        BookDto source = randomizer.nextObject(BookDto.class);
        source.setId(book.getId());
        source.setShelvingId(book.getShelvingId());
        mockMvc.perform(put(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(source)))
                .andDo(print())
                .andExpect(status().isAccepted());
    }

    @Test
    void failPutBookTest() throws Exception {
        BookDto source = randomizer.nextObject(BookDto.class);
        source.setId(book.getId());
        mockMvc.perform(put(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(source)))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(mvcResult -> Objects.requireNonNull(mvcResult.getResolvedException()).getClass().isAssignableFrom(EntityNotFoundException.class));
        ;
    }

}