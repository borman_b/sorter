package ru.sorter.mapper;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import ru.sorter.domain.entity.Book;

import static org.junit.jupiter.api.Assertions.*;

public class BookDtoMapperTest {

    private static EasyRandom randomizer = new EasyRandom();
    private static Book source;

    @Spy
    private BookDtoMapper mapper = Mappers.getMapper(BookDtoMapper.class);

    @BeforeAll
    public static void setup() {
        source = randomizer.nextObject(Book.class);
    }

    @Test
    void mapperTest() {
        var result = mapper.apply(source);
        assertNotNull(result);
        assertEquals(result.getId(), source.getId());
        org.junit.jupiter.api.Assertions.assertAll(
                () -> assertNotNull(result.getLevel()),
                () -> assertNotNull(source.getLevel()),
                () -> assertEquals(result.getLevel().name(), source.getLevel().name()));
        assertEquals(result.getName(), source.getName());
        assertEquals(result.getShelvingId(), source.getShelvingId());
    }

    @Test
    void nullMapperTest() {
        var result = mapper.apply(null);
        assertNull(result);
    }
}
