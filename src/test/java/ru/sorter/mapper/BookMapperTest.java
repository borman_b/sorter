package ru.sorter.mapper;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import ru.sorter.web.dto.BookDto;

import static org.junit.jupiter.api.Assertions.*;

public class BookMapperTest {

    private static EasyRandom randomizer = new EasyRandom();
    private static BookDto source;

    @Spy
    private BookMapper mapper = Mappers.getMapper(BookMapper.class);

    @BeforeAll
    public static void setup() {
        source = randomizer.nextObject(BookDto.class);
    }

    @Test
    void mapperTest() {
        var result = mapper.apply(source);
        assertNotNull(result);
        assertEquals(result.getId(), source.getId());
        org.junit.jupiter.api.Assertions.assertAll(
                () -> assertNotNull(result.getLevel()),
                () -> assertNotNull(source.getLevel()),
                () -> assertEquals(result.getLevel().name(), source.getLevel().name()));
        assertEquals(result.getShelvingId(), source.getShelvingId());
        assertEquals(result.getName(), source.getName());
    }

    @Test
    void nullMapperTest() {
        var result = mapper.apply(null);
        assertNull(result);
    }

}
