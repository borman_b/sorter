package ru.sorter.service.impl;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.Shelving;
import ru.sorter.domain.entity.enums.Level;
import ru.sorter.domain.repository.BookRepository;
import ru.sorter.domain.repository.ShopRepository;
import ru.sorter.exception.EntityNotFoundException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class CommonServiceTest {

    @InjectMocks
    private CommonService service;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private ShopRepository shopRepository;

    private static EasyRandom randomizer = new EasyRandom();
    private static Book book;
    private static Shelving shelving;

    @BeforeAll
    static void setUp() {
        book = randomizer.nextObject(Book.class);
        shelving = randomizer.nextObject(Shelving.class);
        book.setShelvingId(shelving.getId());
        shelving.setBooks(List.of(book));
    }

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getByIdTest() {
        when(bookRepository.findById(eq(book.getId()))).thenReturn(Optional.of(book));
        Book result = service.getById(book.getId());
        assertNotNull(result);
        verify(bookRepository).findById(eq(book.getId()));
    }

    @Test
    void failGetByIdTest() {
        when(bookRepository.findById(any())).thenThrow(EntityNotFoundException.class);
        assertThrows(EntityNotFoundException.class, () -> service.getById(book.getId()));
        verify(bookRepository).findById(eq(book.getId()));
    }

    private static Stream<Arguments> getParams() {
        return Stream.of(
                Arguments.of(shelving.getId(), null),
                Arguments.of(null, book.getLevel()),
                Arguments.of(shelving.getId(), book.getLevel())
        );
    }

    @ParameterizedTest
    @MethodSource("getParams")
    void getByParamsTest(UUID shelvingId, Level level) {
        when(shopRepository.findById(eq(shelving.getId()))).thenReturn(Optional.of(shelving));
        when(bookRepository.findAllByLevel(any())).thenReturn(List.of(book));
        Collection<Book> list = service.getByParams(shelvingId, level);
        assertNotNull(list);
        assertEquals(((List) list).get(0), book);
    }

    @Test
    void getByNameTest() {
        when(bookRepository.findAllByName(any())).thenReturn(List.of(book));
        Book result = service.getByName(book.getName());
        assertNotNull(result);
        assertEquals(result, book);
        verify(bookRepository).findAllByName(eq(book.getName()));
    }

    @Test
    void createTest() {
        when(shopRepository.findById(eq(shelving.getId()))).thenReturn(Optional.of(shelving));
        when(bookRepository.saveAndFlush(any())).thenReturn(book);
        when(shopRepository.saveAndFlush(any())).thenReturn(shelving);
        var result = service.create(book);
        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getShelvingId(), shelving.getId());
        assertEquals(result.getLevel(), book.getLevel());
        assertEquals(result.getName(), book.getName());
        verify(shopRepository, times(2)).findById(eq(shelving.getId()));
        verify(bookRepository).saveAndFlush(any());
        verify(shopRepository).saveAndFlush(any());
    }

    @Test
    void createByNewShelvingTest() {
        Book source = new Book();
        source.setName("Азбука");
        when(shopRepository.findById(any())).thenReturn(Optional.of(shelving));
        when(shopRepository.saveAndFlush(any())).thenReturn(shelving);
        when(bookRepository.saveAndFlush(any())).thenReturn(book);
        var result = service.create(source);
        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getShelvingId(), shelving.getId());
        assertEquals(result.getLevel(), book.getLevel());
        assertEquals(result.getName(), book.getName());
        verify(shopRepository).findById(any());
        verify(bookRepository).saveAndFlush(any());
        verify(shopRepository, times(2)).saveAndFlush(any());
    }

    @Test
    void failCreateTest() {
        Book source = new Book();
        source.setName("Азбука");
        source.setShelvingId(UUID.randomUUID());
        when(bookRepository.saveAndFlush(any())).thenReturn(book);
        assertThrows(EntityNotFoundException.class, () -> service.create(book));
        verify(shopRepository).findById(eq(shelving.getId()));
        verify(bookRepository, never()).saveAndFlush(any());
        verify(shopRepository, never()).saveAndFlush(any());
    }

    @Test
    void deleteTest() {
        doNothing().when(bookRepository).deleteById(any());
        service.delete(book.getId());
        verify(bookRepository).deleteById(eq(book.getId()));
    }

    @Test
    void failDeleteTest() {
        doThrow(EmptyResultDataAccessException.class).when(bookRepository).deleteById(any());
        assertThrows(EntityNotFoundException.class, () -> service.delete(UUID.randomUUID()));
        verify(bookRepository).deleteById(any());
    }

    @Test
    void put() {
        var source = randomizer.nextObject(Book.class);
        source.setShelvingId(shelving.getId());
        when(bookRepository.findById(eq(book.getId()))).thenReturn(Optional.of(book));
        when(bookRepository.saveAndFlush(any())).thenReturn(book);
        when(shopRepository.saveAndFlush(any())).thenReturn(shelving);
        when(shopRepository.findById(eq(shelving.getId()))).thenReturn(Optional.of(shelving));
        var result = service.createOrUpdate(source);
        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getShelvingId(), shelving.getId());
        assertEquals(result.getLevel(), book.getLevel());
        assertEquals(result.getName(), book.getName());
        verify(shopRepository, times(2)).findById(any());
        verify(bookRepository).saveAndFlush(any());
        verify(shopRepository).saveAndFlush(any());
    }
}
