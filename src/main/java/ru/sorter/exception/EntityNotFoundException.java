package ru.sorter.exception;

import java.util.UUID;
import java.util.function.Supplier;

public class EntityNotFoundException extends ServiceException {

    public EntityNotFoundException(String cause) {
        super(cause);
    }

    public static Supplier<EntityNotFoundException> forBook(UUID bookId) {
        return () -> new EntityNotFoundException("Book not found for id = " + bookId);
    }

    public static Supplier<EntityNotFoundException> forBookByName(String name) {
        return () -> new EntityNotFoundException("Book not found for name = " + name);
    }

    public static Supplier<EntityNotFoundException> forShelving(UUID shelvingId) {
        return () -> new EntityNotFoundException("Shelving not found for id = " + shelvingId);
    }
}
