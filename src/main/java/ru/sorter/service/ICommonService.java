package ru.sorter.service;


import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.enums.Level;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface ICommonService {
    Book getById(UUID bookId);

    Collection<Book> getByParams(UUID shelvingId, Level level);

    Book getByName(String name);

    Book create(Book book);

    void delete(UUID bookId);

    Book createOrUpdate(Book book);
}
