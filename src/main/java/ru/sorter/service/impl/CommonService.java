package ru.sorter.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.Shelving;
import ru.sorter.domain.entity.enums.Level;
import ru.sorter.domain.repository.BookRepository;
import ru.sorter.domain.repository.ShopRepository;
import ru.sorter.exception.EntityNotFoundException;
import ru.sorter.service.ICommonService;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.function.BiConsumer;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class CommonService implements ICommonService {

    private final BookRepository bookRepository;
    private final ShopRepository shopRepository;

    BiConsumer<Book, Shelving> shelvingConsumer = (book, shelving) -> Optional.of(shelving)
            .map(Shelving::getId)
            .ifPresent(book::setShelvingId);

    @Override
    public Book getById(UUID bookId) {
        return bookRepository.findById(bookId).orElseThrow(EntityNotFoundException.forBook(bookId));
    }

    @Override
    public Collection<Book> getByParams(UUID shelvingId, Level level) {
        return Optional.ofNullable(shelvingId)
                .map(this::getShelvingById)
                .map(Shelving::getBooks)
                .orElseGet(() -> bookRepository.findAllByLevel(level))
                .stream()
                .filter(it -> level == null || level.equals(it.getLevel()))
                .collect(toList());
    }


    @Override
    public Book getByName(String name) {
        return bookRepository.findAllByName(name)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException.forBookByName(name));
    }

    @Override
    public Book create(@NotNull Book book) {
        Optional.ofNullable(book.getShelvingId())
                .ifPresentOrElse(it -> shelvingConsumer.accept(book, getShelvingById(it)),
                        () -> shelvingConsumer.accept(book, createShelving()));

        if (book.getLevel() == null) {
            book.setLevel(Level.randomLevel());
        }

        var result = bookRepository.saveAndFlush(book);

        updateShelving(result);
        return result;
    }

    private void updateShelving(@NotNull Book book) {
        Shelving shelving = getShelvingById(book.getShelvingId());
        List books = new ArrayList<>(shelving.getBooks());
        books.add(book);
        shelving.setBooks(books);
        shopRepository.saveAndFlush(shelving);
    }

    private Shelving createShelving() {
        return shopRepository.saveAndFlush(new Shelving());
    }

    @Override
    @Transactional
    public void delete(UUID bookId) {
        try {
            bookRepository.deleteById(bookId);
        } catch (EmptyResultDataAccessException ex) {
            throw EntityNotFoundException.forBook(bookId).get();
        }
    }

    @Override
    @Transactional
    public Book createOrUpdate(@NotNull Book book) {
        Optional.ofNullable(book.getId())
                .ifPresent(this::delete);
        book.setId(null);
        return create(book);
    }

    private Shelving getShelvingById(UUID shelvingId) {
        return shopRepository.findById(shelvingId).orElseThrow(EntityNotFoundException.forShelving(shelvingId));
    }
}
