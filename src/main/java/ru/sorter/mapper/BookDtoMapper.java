package ru.sorter.mapper;

import org.mapstruct.Mapper;
import ru.sorter.domain.entity.Book;
import ru.sorter.web.dto.BookDto;

import java.util.function.Function;

@Mapper(componentModel = "spring")
public interface BookDtoMapper extends Function<Book, BookDto> {
}
