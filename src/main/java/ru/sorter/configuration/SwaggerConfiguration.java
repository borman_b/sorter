package ru.sorter.configuration;

import java.util.Collections;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.StringVendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    @Bean
    public Docket swagger() {
        Contact contact = new Contact("Open source for test", "http://www.ya.ry/", "noreply@gmail.com");
        ApiInfo apiInfo = new ApiInfo("MS Sorter",
                "Microservice booking RESTful API ",
                "0.0.1",
                null,
                contact,
                null,
                null,
                Collections.singletonList(new StringVendorExtension("name", "value")));
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ru.sorter.web.controller"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .apiInfo(apiInfo);
    }
}
