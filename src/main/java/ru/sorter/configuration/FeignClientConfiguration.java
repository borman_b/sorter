package ru.sorter.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@Import(FeignClientsConfiguration.class)
@EnableFeignClients()
@Configuration
@RequiredArgsConstructor
public class FeignClientConfiguration {
}
