package ru.sorter.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.sorter.mapper.BookDtoMapper;
import ru.sorter.mapper.BookMapper;
import ru.sorter.service.ICommonService;
import ru.sorter.web.client.BookingApi;
import ru.sorter.web.dto.BookDto;
import ru.sorter.web.dto.enums.Level;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
public class BookingController implements BookingApi {

    private final ICommonService commonService;
    private final BookDtoMapper bookDtoMapper;
    private final BookMapper bookMapper;

    @Override
    public HttpEntity<BookDto> getBook(UUID bookId) {
        var result = commonService.getById(bookId);
        return ResponseEntity.ok(bookDtoMapper.apply(result));
    }

    @Override
    public HttpEntity<Collection<BookDto>> getBooksByParams(UUID shelvingId, Level level) {
        var calcLevel = Optional.ofNullable(level)
                .map(Enum::name)
                .map(ru.sorter.domain.entity.enums.Level::byName)
                .orElse(null);
        Collection<BookDto> result = commonService.getByParams(shelvingId, calcLevel)
                .stream()
                .map(bookDtoMapper)
                .collect(toList());
        return ResponseEntity.ok(result);
    }

    @Override
    public HttpEntity<BookDto> getBookByName(@NotNull @RequestParam(value = "value") String name) {
        var result = commonService.getByName(name);
        return ResponseEntity.ok(bookDtoMapper.apply(result));
    }

    @Override
    public HttpEntity<BookDto> createBook(@NotNull @Valid BookDto book) {
        var result = commonService.create(bookMapper.apply(book));
        return new ResponseEntity(bookDtoMapper.apply(result), HttpStatus.CREATED);
    }

    @Override
    public HttpEntity deleteBook(@PathVariable UUID bookId) {
        commonService.delete(bookId);
        return ResponseEntity.accepted().build();
    }

    @Override
    public HttpEntity<BookDto> putBook(@NotNull @Valid @RequestBody BookDto book) {
        var result = commonService.createOrUpdate(bookMapper.apply(book));
        return new ResponseEntity(bookDtoMapper.apply(result), HttpStatus.ACCEPTED);
    }

}
