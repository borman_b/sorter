package ru.sorter.web.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "booking")
public interface BookingFeignClient extends BookingApi {
}
