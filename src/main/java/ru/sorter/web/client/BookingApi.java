package ru.sorter.web.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.sorter.web.dto.BookDto;
import ru.sorter.web.dto.enums.Level;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.UUID;

@RequestMapping("/booking/api/v1/books")
public interface BookingApi {

    @GetMapping("/{bookId}")
    HttpEntity<BookDto> getBook(@PathVariable UUID bookId);

    @GetMapping()
    HttpEntity<Collection<BookDto>> getBooksByParams(
            @RequestParam(value = "shelvingId", required = false) UUID shelvingId,
            @RequestParam(value = "level", required = false) Level level);

    @GetMapping("/name")
    HttpEntity<BookDto> getBookByName(@RequestParam(value = "value") String name);

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    HttpEntity createBook(@NotNull @Valid @RequestBody BookDto book);

    @DeleteMapping("/{bookId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    HttpEntity deleteBook(@PathVariable UUID bookId);

    @PutMapping()
    @ResponseStatus(HttpStatus.ACCEPTED)
    HttpEntity<BookDto> putBook(@NotNull @Valid @RequestBody BookDto book);

}