package ru.sorter.web.dto.enums;

public enum Level {
    LOW,
    MIDDLE,
    HIGH
}
