package ru.sorter.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

@Entity
@Table(name = "shelving")
@Data
@EqualsAndHashCode(callSuper = false)
public class Shelving extends EntityWithTimestamps {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Type(type = "uuid-char")
    @Column(name = "shelving_id")
    private UUID id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "shelving2book",
            joinColumns = @JoinColumn(name = "shelving_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private Collection<Book> books;
}
