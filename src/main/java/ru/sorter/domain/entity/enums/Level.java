package ru.sorter.domain.entity.enums;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public enum Level {
    LOW,
    MIDDLE,
    HIGH;

    private static final Map<String, Level> strToEnum = Stream.of(values()).collect(toMap(Level::name, x -> x));

    public static Optional<Level> getOptionalByName(String name) {
        return Optional.ofNullable(strToEnum.get(name));
    }

    public static Level byName(String name) {
        return getOptionalByName(name)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find Level for name:" + name));
    }

    private static final List<Level> values = Stream.of(values()).collect(toList());

    private static final Random random = new Random();

    public static Level randomLevel() {
        return values.get(random.nextInt(values.size()));
    }

}
