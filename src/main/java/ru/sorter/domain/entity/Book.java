package ru.sorter.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import ru.sorter.domain.entity.enums.Level;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "book")
@Data
@EqualsAndHashCode(callSuper = false)
public class Book extends EntityWithTimestamps {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Type(type = "uuid-char")
    @Column(name = "book_id")
    private UUID id;

    private String name;
    @Enumerated(value = EnumType.STRING)
    private Level level;

    @Type(type = "uuid-char")
    private UUID shelvingId;
}
