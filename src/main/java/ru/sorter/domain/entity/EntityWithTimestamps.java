package ru.sorter.domain.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class EntityWithTimestamps implements Serializable {

    @CreationTimestamp
    @JsonIgnore
    @Column(name = "load_date", updatable = false)
    private LocalDateTime loadDate;

    @UpdateTimestamp
    @JsonIgnore
    @Column(name = "update_date")
    private LocalDateTime updateDate;
}

