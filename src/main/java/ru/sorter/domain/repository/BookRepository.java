package ru.sorter.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.enums.Level;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<Book, UUID> {

    Optional<Book> findById(UUID bookId);

    Collection<Book> findAllByLevel(Level level);

    Collection<Book> findAllByName(String name);

}
