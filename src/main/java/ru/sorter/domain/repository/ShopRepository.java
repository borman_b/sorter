package ru.sorter.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sorter.domain.entity.Book;
import ru.sorter.domain.entity.Shelving;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ShopRepository extends JpaRepository<Shelving, UUID> {
    Optional<Shelving> findById(UUID shelvingId);
}
